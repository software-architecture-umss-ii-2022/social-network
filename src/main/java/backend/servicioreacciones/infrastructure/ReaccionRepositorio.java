package backend.servicioreacciones.infrastructure;

import backend.servicioreacciones.Reaccion;
import backend.servicioreacciones.Emocion;
import java.util.List;

public interface ReaccionRepositorio {

  void saveWithUserIdAndReactionAndPublicationId(int userId, Emocion emocion, int publicationId);
  List<Reaccion> getAll();
}
