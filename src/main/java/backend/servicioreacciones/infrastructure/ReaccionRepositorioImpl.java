package backend.servicioreacciones.infrastructure;

import backend.servicioreacciones.Emocion;
import backend.servicioreacciones.Reaccion;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReaccionRepositorioImpl implements ReaccionRepositorio {

  @Override
  public void saveWithUserIdAndReactionAndPublicationId(int userId, Emocion emocion,
      int publicationId) {
    try {
      FileWriter output = new FileWriter("Reacciones.csv", true);
      output.write(String.format("%s,%s,%s\n", publicationId, emocion, userId));
      output.flush();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Reaccion> getAll() {
    List<Reaccion> reaccions = new ArrayList<>();
    Scanner input;
    try {
      input = new Scanner(new File("Reacciones.csv"));
    } catch (IOException e) {
      throw new RuntimeException("Error finding ReactionInstances.csv file");
    }
    while (input.hasNextLine()) {
      String row = input.nextLine();
      String[] reactionInstanceData = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
      int publicationId = Integer.parseInt(reactionInstanceData[0]);
      Emocion emocion = Emocion.valueOf(reactionInstanceData[1]);
      int userId = Integer.parseInt(reactionInstanceData[2]);

      reaccions.add(new Reaccion(userId, publicationId, emocion));
    }
    return reaccions;
  }
}
