package backend.servicioreacciones;

import backend.servicioreacciones.infrastructure.ReaccionRepositorio;
import backend.servicioreacciones.application.ReaccionCaseDeUso;
import backend.servicioreacciones.infrastructure.ReaccionRepositorioImpl;
import java.util.Map;

public class ServicioReacciones {

  private final ReaccionCaseDeUso reaccionCaseDeUso;

  public ServicioReacciones() {
    ReaccionRepositorio reaccionRepositorio = new ReaccionRepositorioImpl();

    this.reaccionCaseDeUso = new ReaccionCaseDeUso(reaccionRepositorio);
  }

  public void agregarReaccion(int idPublicacion, int idUsuario, Emocion reaccion) {
    reaccionCaseDeUso.guardarReactionInstance(idUsuario, reaccion, idPublicacion);
  }

  public Map<Emocion, Integer> listarResumenReacciones(int idPublicacion) {
    return reaccionCaseDeUso.obtenerInformacionReacionesDePublicacion(idPublicacion);
  }

  /*
  public List<Reaccion> obtenerReaccionesDePublicacion(int idPublicacion) {
    return reaccionCaseDeUso.obtenerReactionesDePublication(idPublicacion);
  }
   */
}
