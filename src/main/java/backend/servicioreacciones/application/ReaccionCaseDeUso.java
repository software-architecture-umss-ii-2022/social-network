package backend.servicioreacciones.application;

import backend.servicioreacciones.infrastructure.ReaccionRepositorio;
import backend.serviciopublicaciones.Publicacion;
import backend.servicioreacciones.Reaccion;
import backend.servicioreacciones.Emocion;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReaccionCaseDeUso {

  private final ReaccionRepositorio repository;

  public ReaccionCaseDeUso(ReaccionRepositorio reaccionRepositorio) {
    this.repository = reaccionRepositorio;
  }

  public void guardarReactionInstance(int idUsuario, Emocion emocion, int idPublicacion) {
    repository.saveWithUserIdAndReactionAndPublicationId(idUsuario, emocion, idPublicacion);
  }

  public Map<Emocion, Integer> obtenerInformacionReacionesDePublicacion(int idPublicacion) {
    Map<Emocion, Integer> reaccionesCantidad = new HashMap<>();
    Arrays.asList(Emocion.values())
        .forEach(emocion -> reaccionesCantidad.put(emocion, 0));

    obtenerReactionesDePublication(idPublicacion)
        .forEach(reaccion ->
            reaccionesCantidad
                .put(reaccion.getEmocion(), reaccionesCantidad.get(reaccion.getEmocion()) + 1)
        );
    /*
    obtenerReactionesDePublication(idPublicacion).stream()
        .collect(Collectors.groupingBy(reaccion -> reaccion.getEmocion()))
        .forEach(((emocion, reaccions) -> reaccionesCantidad.put(emocion, reaccions.size())));
    */
    return reaccionesCantidad;
  }

  public List<Reaccion> obtenerReactionesDePublication(int idPublicacion) {
      return repository.getAll().stream()
          .filter(reactionInstance ->
              reactionInstance.getIdPublicacion() == idPublicacion
          )
          .collect(Collectors.toList());
  }
}
