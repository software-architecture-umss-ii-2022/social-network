package backend.servicioreacciones;

public class Reaccion {

  private final int idUsuario;
  private final int idPublicacion;

  private final Emocion emocion;

  public Reaccion(int idUsuario, int idPublicacion, Emocion emocion) {
    this.idUsuario = idUsuario;
    this.idPublicacion = idPublicacion;
    this.emocion = emocion;
  }

  public int getIdUsuario() {
    return idUsuario;
  }

  public Emocion getEmocion() {
    return emocion;
  }

  public int getIdPublicacion() {
    return idPublicacion;
  }
}
