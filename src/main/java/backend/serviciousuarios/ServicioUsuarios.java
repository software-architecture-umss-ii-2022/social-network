package backend.serviciousuarios;

import backend.serviciousuarios.infrastructure.UsuarioRepositorio;
import backend.serviciousuarios.infrastructure.UsuarioRepositorioImpl;
import java.util.List;

public class ServicioUsuarios {

  private final UsuarioRepositorio usuarioRepositorio;

  public ServicioUsuarios() {
    this.usuarioRepositorio = new UsuarioRepositorioImpl();
  }

  public int agregarUsuario(String nombre) {
    return usuarioRepositorio.saveUserWithName(nombre);
  }

  public void eliminarUsuario(String nombre) {
    throw new UnsupportedOperationException();
  }

  public Usuario buscarUsuario(int id) {
    return usuarioRepositorio.findById(id).orElse(null);
  }

  public List<Integer> listarUsuarios() {
    return usuarioRepositorio.getAllIDs();
  }

  public void cambiarAUsuario(int id) {
    buscarUsuario(id).cambiarAUsuario();
    usuarioRepositorio.changeUser(id, TipoUsuario.USUARIO);
  }
}
