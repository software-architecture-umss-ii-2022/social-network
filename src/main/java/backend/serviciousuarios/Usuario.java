package backend.serviciousuarios;

public class Usuario {
  private final int id;
  private final String nombre;
  private TipoUsuario estado;

  public Usuario(int id, String nombre) {
    this.id = id;
    this.nombre = nombre;
    this.estado = TipoUsuario.CANDIDATO;
  }

  public String getNombre() {
    return nombre;
  }

  @Override
  public boolean equals(Object o) {
    Usuario other = (Usuario) o;
    return this.id == other.id;
  }

  public boolean esUsuario() {
    return estado == TipoUsuario.USUARIO;
  }

  public void cambiarAUsuario() {
    this.estado = TipoUsuario.USUARIO;
    System.out.printf("El usuario %s cambió de CANDIDATO a USUARIO\n%n", nombre);
  }
}
