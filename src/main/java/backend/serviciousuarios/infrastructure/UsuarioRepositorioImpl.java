package backend.serviciousuarios.infrastructure;

import backend.serviciousuarios.TipoUsuario;
import backend.serviciousuarios.Usuario;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class UsuarioRepositorioImpl implements UsuarioRepositorio {

  private final SortedMap<Integer, Entry<Usuario, TipoUsuario>> users;
  private final File usersFile;
  private final Scanner input;

  public UsuarioRepositorioImpl() {
    this.users = new TreeMap<>();
    this.usersFile = new File("Usuarios.csv");
    try {
      this.input = new Scanner(usersFile);
      uploadData();
    } catch (FileNotFoundException exception) {
      throw new RuntimeException("Error finding Usuarios.csv file");
    }
  }

  @Override
  public int saveUserWithName(String name) {
    return searchUserIdByName(name).orElseGet(() -> saveNewUser(name));
  }

  public Optional<Usuario> findById(int id) {
    try {
      return Optional.of(users.get(id).getKey());
    } catch (NullPointerException exception) {
      return Optional.empty();
    }
  }

  //We need to improve this method added get TipoUsuario in Usuario entity.
  @Override
  public void changeUser(int id, TipoUsuario type) {
    users.get(id).setValue(type);
    saveUsersInFile();
  }

  @Override
  public List<Integer> getAllIDs() {
    return new ArrayList<>(users.keySet());
  }

  private void uploadData() {
    while (input.hasNextLine()) {
      String row = input.nextLine();
      String[] userData = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
      int userId = Integer.parseInt(userData[0]);
      String userName = userData[1];
      TipoUsuario type = TipoUsuario.valueOf(userData[2]);
      Usuario user = new Usuario(userId, userName);
      users.put(userId, new SimpleEntry<>(user, type));
    }
  }

  private Optional<Integer> searchUserIdByName(String name) {
    return users.entrySet()
        .stream()
        .filter(pair -> pair.getValue().getKey().getNombre().equals(name))
        .map(Entry::getKey)
        .findFirst();
  }

  private Integer saveNewUser(String name) {
    Integer userId = users.lastKey() + 1;
    Usuario user = new Usuario(userId, name);
    users.put(userId, new SimpleEntry<>(user, TipoUsuario.CANDIDATO));
    saveUsersInFile();
    return userId;
  }

  public void saveUsersInFile() {
    try {
      FileWriter output = new FileWriter(usersFile);
      users.forEach((id, value) -> {
        try {
          output.write(
              String.format("%s,%s,%s\n", id, value.getKey().getNombre(), value.getValue())
          );
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
      output.close();
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
}
