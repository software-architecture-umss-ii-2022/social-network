package backend.serviciousuarios.infrastructure;

import backend.serviciousuarios.TipoUsuario;
import backend.serviciousuarios.Usuario;
import java.util.List;
import java.util.Optional;

public interface UsuarioRepositorio {
  int saveUserWithName(String name);
  void changeUser(int id, TipoUsuario type);
  Optional<Usuario> findById(int id);
  List<Integer> getAllIDs();

  void saveUsersInFile();
}
