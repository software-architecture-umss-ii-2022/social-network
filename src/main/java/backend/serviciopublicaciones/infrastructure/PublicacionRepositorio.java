package backend.serviciopublicaciones.infrastructure;

import backend.serviciopublicaciones.Publicacion;
import java.util.List;
import java.util.Optional;

public interface PublicacionRepositorio {

  int saveWithUserIdAndContent(int userId, String content);
  Optional<Publicacion> edit(int id, Publicacion publicacion);
  Optional<Publicacion> findById(int id);
  Optional<Publicacion> deleteById(int id);
  List<Publicacion> getAll();
  List<Integer> getAllSortedIds();
}
