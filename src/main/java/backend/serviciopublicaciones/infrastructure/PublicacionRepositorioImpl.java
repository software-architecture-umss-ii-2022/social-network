package backend.serviciopublicaciones.infrastructure;

import backend.serviciopublicaciones.Publicacion;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

public class PublicacionRepositorioImpl implements PublicacionRepositorio {

  @Override
  public int saveWithUserIdAndContent(int userId, String content) {
    try {
      FileWriter output = new FileWriter("Publicaciones.csv", true);
      List<Publicacion> lista = getAll();
      int id = 1;
      if (!lista.isEmpty()) {
        id = lista.size() + 1;
      }
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
      output.write(
          String.format(
              "%s,%s,\"%s\",%s\n",
              id,
              userId,
              content,
              simpleDateFormat.format(new Date())
          )
      );
      output.flush();
      return id;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Publicacion> edit(int id, Publicacion publicacion) {
    return null;
  }

  @Override
  public Optional<Publicacion> findById(int id) {
    return getAll().stream()
        .filter(publication -> publication.equals(new Publicacion(id, 0, null, null)))
        .findFirst();
  }

  @Override
  public Optional<Publicacion> deleteById(int id) {
    return getAll().stream()
        .filter(publication -> publication.equals(new Publicacion(id, 0, null, null)))
        .findFirst();
  }

  @Override
  public List<Publicacion> getAll() {
    List<Publicacion> publicacions = new ArrayList<>();
    Scanner input;
    try {
      input = new Scanner(new File("Publicaciones.csv"));
    } catch (IOException e) {
      throw new RuntimeException("Error finding Publicaciones.csv file");
    }
    while (input.hasNextLine()) {
      String row = input.nextLine();
      String[] publicationData = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
      int id = Integer.parseInt(publicationData[0]);
      int userId = Integer.parseInt(publicationData[1]);
      String content = publicationData[2].substring(1, publicationData[2].length() - 1);
      String fecha = publicationData[3];
      publicacions.add(new Publicacion(id, userId, content, fecha));
    }
    return publicacions;
  }

  @Override
  public List<Integer> getAllSortedIds() {
    List<Integer> publicacionesIds = new ArrayList<>();
    Scanner input;
    try {
      input = new Scanner(new File("Publicaciones.csv"));
    } catch (IOException e) {
      throw new RuntimeException("Error finding Publicaciones.csv file");
    }
    HashMap<Integer, Date> map = new HashMap<>();
    while (input.hasNextLine()) {
      String row = input.nextLine();
      String[] publicacionData = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
      int idPublicacion = Integer.parseInt(publicacionData[0]);
      try {
        Date publicationDate = new SimpleDateFormat("dd/MM/yyyy").parse(publicacionData[3]);
        map.put(idPublicacion, publicationDate);
      } catch (ParseException e) {
          throw new RuntimeException("Error Parsing date");
      }
    }
    map.entrySet().stream()
        .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
        .forEach(value -> publicacionesIds.add(value.getKey()));
    return publicacionesIds;
  }
}
