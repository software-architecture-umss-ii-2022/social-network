package backend.serviciopublicaciones;

import backend.serviciopublicaciones.infrastructure.PublicacionRepositorio;
import backend.serviciopublicaciones.application.PublicacionCasoDeUso;
import backend.serviciopublicaciones.infrastructure.PublicacionRepositorioImpl;
import java.util.List;

public class ServicioPublicaciones {

  private final PublicacionCasoDeUso publicacionCasoDeUso;

  public ServicioPublicaciones() {
    PublicacionRepositorio publicacionRepositorio = new PublicacionRepositorioImpl();
    this.publicacionCasoDeUso = new PublicacionCasoDeUso(publicacionRepositorio);
  }

  public int agregarPublicacion(int idUsuario, String contenidoPublication) {
    return publicacionCasoDeUso.guardarPublicacionConIdUsuarioYContenido(idUsuario, contenidoPublication);
  }

  public Publicacion buscarPublicacion(int idPublicacion) {
    return publicacionCasoDeUso.obtenerPublicacionPorId(idPublicacion);
  }

  public List<Integer> listarPublicaciones() {
    return publicacionCasoDeUso.obtenerIdsPublicaciones();
  }
}
