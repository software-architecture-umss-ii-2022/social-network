package backend.serviciopublicaciones;

public class Publicacion {
  private final int idPublicacion;
  private final int idUsuario;
  private final String contenido;
  private final String fecha;

  public Publicacion(int idPublicacion, int idUsuario, String contenido, String fecha) {
    this.idPublicacion = idPublicacion;
    this.idUsuario = idUsuario;
    this.contenido = contenido;
    this.fecha = fecha;
  }

  public int getIdUsuario() {
    return idUsuario;
  }

  public String getContenido() {
    return contenido;
  }

  public String getFecha() {
    return fecha;
  }

  @Override
  public boolean equals(Object o) {
    Publicacion other = (Publicacion) o;
    return this.idPublicacion == other.idPublicacion;
  }
}
