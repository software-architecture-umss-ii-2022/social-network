package backend.serviciopublicaciones.application;

import backend.serviciopublicaciones.infrastructure.PublicacionRepositorio;
import backend.serviciopublicaciones.Publicacion;
import java.util.List;

public class PublicacionCasoDeUso {

  private final PublicacionRepositorio repository;

  public PublicacionCasoDeUso(PublicacionRepositorio repository) {
    this.repository = repository;
  }

  public int guardarPublicacionConIdUsuarioYContenido(int idUsuario, String contenido) {
    return repository.saveWithUserIdAndContent(idUsuario, contenido);
  }

  /*
  public List<Publicacion> getAllPublications() {
    return repository.getAll();
  }
   */

  public List<Integer> obtenerIdsPublicaciones() {
    return repository.getAllSortedIds();
  }

  public Publicacion obtenerPublicacionPorId(int id) {
    return repository.findById(id).orElseThrow(() -> new RuntimeException("Publication not Found"));
  }
}
