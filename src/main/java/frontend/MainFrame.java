package frontend;

import frontend.pages.authenticationPage.PaginaAutentificacion;
import javax.swing.JFrame;

public class MainFrame extends JFrame {
  private final int width;
  private final int height;

  public MainFrame(int width, int height) {
    this.width = width;
    this.height = height;
    configureFrame();
  }

  public MainFrame() {
    this.width = 720;
    this.height = 480;
    configureFrame();
  }

  public void configureFrame() {

    setTitle("social-network");
    setSize(width, height);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    getContentPane().add(new PaginaAutentificacion());

    setLocationRelativeTo(null);
    setResizable(false);
    setVisible(true);
  }
}
