package frontend.global;

import backend.serviciopublicaciones.ServicioPublicaciones;
import backend.servicioreacciones.ServicioReacciones;
import backend.serviciousuarios.ServicioUsuarios;

public class Accion {

  private final static Accion accionInstance = new Accion();
  private ServicioPublicaciones servicioPublicaciones;
  private ServicioUsuarios servicioUsuarios;
  private ServicioReacciones servicioReacciones;

  private Accion() {}

  public static Accion getInstance() {
    return accionInstance;
  }

  public void setServicioPublicaciones(ServicioPublicaciones servicioPublicaciones) {
    this.servicioPublicaciones = servicioPublicaciones;
  }

  public void setServicioUsuarios(ServicioUsuarios servicioUsuarios) {
    this.servicioUsuarios = servicioUsuarios;
  }

  public void setServicioReacciones(ServicioReacciones servicioReacciones) {
    this.servicioReacciones = servicioReacciones;
  }

  public ServicioPublicaciones getServicioPublicaciones() {
    return servicioPublicaciones;
  }

  public ServicioUsuarios getServicioUsuarios() {
    return servicioUsuarios;
  }

  public ServicioReacciones getServicioReacciones() {
    return servicioReacciones;
  }
}
