package frontend.global;

public class SesionActual {

  private static SesionActual instance = null;
  private int currentUsuario;

  private SesionActual() {}

  public static SesionActual getInstance() {
    if (instance == null)
      instance = new SesionActual();
    return instance;
  }

  public int getCurrentUser() {
    return currentUsuario;
  }

  public void setCurrentUser(int usuarioId) {
    this.currentUsuario = usuarioId;
  }
}
