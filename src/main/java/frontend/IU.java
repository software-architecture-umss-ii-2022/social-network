package frontend;

import backend.serviciopublicaciones.ServicioPublicaciones;
import backend.servicioreacciones.ServicioReacciones;
import backend.serviciousuarios.ServicioUsuarios;
import frontend.global.Accion;

public class IU {

  public IU(
      ServicioPublicaciones servicioPublicaciones,
      ServicioReacciones servicioReacciones,
      ServicioUsuarios servicioUsuarios
      ) {
    Accion.getInstance().setServicioPublicaciones(servicioPublicaciones);
    Accion.getInstance().setServicioReacciones(servicioReacciones);
    Accion.getInstance().setServicioUsuarios(servicioUsuarios);
  }

  public void iniciar() {
    MainFrame mainFrame = new MainFrame();
  }
}
