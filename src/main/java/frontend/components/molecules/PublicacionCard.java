package frontend.components.molecules;

import backend.servicioreacciones.Emocion;
import backend.serviciopublicaciones.Publicacion;
import backend.serviciousuarios.ServicioUsuarios;
import backend.serviciousuarios.Usuario;
import frontend.components.atoms.ContenidoBasico;
import frontend.global.Accion;
import frontend.global.SesionActual;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PublicacionCard extends JPanel {

  private final int publicacionId;
  private final Publicacion publicacion;
  private final ContenidoBasico content;

  private ButtonGroup checkBoxGroup;
  private Emocion checkBoxValue;
  public PublicacionCard(int publicacionId) {
    this.publicacionId = publicacionId;
    this.publicacion = Accion.getInstance().getServicioPublicaciones()
        .buscarPublicacion(publicacionId);
    this.content =
        new ContenidoBasico(
            publicacion.getContenido(),
            500,
            100,
            true
        );

    setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder()));
    configurePanel();
  }

  private void configurePanel() {
    setLayout(new BorderLayout());
    add(createHeader(), BorderLayout.NORTH);
    add(content, BorderLayout.CENTER);
    add(createActionSection(), BorderLayout.SOUTH);
  }

  private JPanel createHeader() {
    JPanel panel = new JPanel(new BorderLayout());
    panel.add(
        new JLabel(
            Accion.getInstance().getServicioUsuarios()
                .buscarUsuario(publicacion.getIdUsuario()).getNombre()
        ),
        BorderLayout.WEST
    );
    panel.add(new JLabel(publicacion.getFecha()), BorderLayout.EAST);
    return panel;
  }
  private JPanel createActionSection() {
    JPanel panel = new JPanel(new BorderLayout());
    panel.add(publicationReactionsButton(), BorderLayout.NORTH);
    JButton button = new JButton("React");
    ActionListener listener = e -> {
      JOptionPane.showMessageDialog(
          this,
          reactionButtons(),
          "Choose a reaction",
          JOptionPane.PLAIN_MESSAGE
      );
      Accion.getInstance().getServicioReacciones()
          .agregarReaccion(
              publicacionId,
              SesionActual.getInstance().getCurrentUser(),
              (checkBoxValue == null)? Emocion.Like : checkBoxValue
          );
      int repetitions = (int) Accion.getInstance().getServicioReacciones()
          .listarResumenReacciones(publicacionId)
          .values().stream().filter(quantity -> quantity > 0).count();
      ServicioUsuarios servicioUsuarios = Accion.getInstance().getServicioUsuarios();
      Usuario user = servicioUsuarios.buscarUsuario(publicacion.getIdUsuario());
      if (repetitions >= 3 && !user.esUsuario()) servicioUsuarios
          .cambiarAUsuario(publicacion.getIdUsuario());
    };
    button.addActionListener(listener);
    panel.add(button, BorderLayout.CENTER);
    return panel;
  }

  private JButton publicationReactionsButton() {
    JButton button = new JButton("Reactions");
    ActionListener listener = e -> {
      JOptionPane.showMessageDialog(
          this,
          publicationReactions(),
          "Users Reactions",
          JOptionPane.PLAIN_MESSAGE
      );
    };
    button.addActionListener(listener);
    return button;
  }

  private JPanel publicationReactions() {
    JPanel panel = new JPanel(new GridLayout(0, 1));
    Accion.getInstance().getServicioReacciones()
        .listarResumenReacciones(publicacionId)
        .forEach((emocion, cantidad) -> {
          URL imageUrl = getClass().getClassLoader().getResource(String.format("%s.png", emocion));
          if (cantidad > 0) {
            if (imageUrl != null) {
              ImageIcon imageIcon = new ImageIcon(imageUrl);
              Image image = imageIcon.getImage();
              Image newImage = image.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
              imageIcon = new ImageIcon(newImage);
              JLabel iconLabel = new JLabel(imageIcon);
              JLabel text = new JLabel(String.format("(%s)", cantidad));
              Box imageText = Box.createHorizontalBox();
              imageText.add(iconLabel);
              imageText.add(text);
              panel.add(imageText);
            } else {
              panel.add(
                  new JLabel(
                      String.format(
                          "%s: %s",
                          emocion,
                          cantidad)
                  )
              );
            }
          }
        });
    return panel;
  }

  private JPanel reactionButtons() {
    JPanel panel = new JPanel(new GridLayout(0, 1));

    checkBoxGroup = new ButtonGroup();
    Arrays.stream(Emocion.values()).forEach(emocion -> {
      URL imageUrl = getClass().getClassLoader().getResource(String.format("%s.png", emocion));
      if (imageUrl != null) {
        ImageIcon imageIcon = new ImageIcon(imageUrl);
        Image image = imageIcon.getImage();
        Image newImage = image.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newImage);

        //JCheckBox checkBox = new JCheckBox(imageIcon, false);
        JCheckBox checkBox = new JCheckBox(String.format(" %s", emocion));
        checkBox.setIcon(imageIcon);
        checkBox.addActionListener(e -> {
          checkBoxValue = emocion;
        });
        panel.add(checkBox);
      }
    });
    return panel;
  }
}
