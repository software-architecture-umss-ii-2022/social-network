package frontend.components.atoms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ContenidoBasico extends JPanel {

  private String src;
  private int width;
  private int height;
  private Color backgroundColor;
  private boolean centered;

  public ContenidoBasico(String src, int width, int height, boolean centered) {
    this.src = src;
    this.width = width;
    this.height = height;
    this.centered = centered;
    this.backgroundColor = new Color(41, 183, 202);
    configurePanel();
  }

  private void configurePanel() {
    setBackground(backgroundColor);
    setLayout(new BorderLayout());

    setPreferredSize(new Dimension(width, height));
    JLabel content = new JLabel(src);
    content.setHorizontalAlignment(JLabel.CENTER);
    add(content);
  }
}
