package frontend.components.organisms;

import frontend.components.molecules.PublicacionCard;
import frontend.global.Accion;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ListaPublicacionCard extends JPanel {

  private final Color backgroundColor;
  //private final int usuario;

  public ListaPublicacionCard(int usuario) {
    //this.usuario = usuario;
    this.backgroundColor = Color.GRAY;
    configurePanel();
  }

  private void configurePanel() {
    setLayout(new BorderLayout());
    add(createList(), BorderLayout.CENTER);
    setBackground(backgroundColor);
  }

  public JScrollPane createList() {
    JPanel panel = new JPanel(new GridLayout(0, 1));
    Accion.getInstance().getServicioPublicaciones()
        .listarPublicaciones()
        .forEach(publicationId -> panel.add(new PublicacionCard(publicationId)));
    JScrollPane scroll = new JScrollPane(panel);
    scroll.setPreferredSize(new Dimension(540, 385));
    scroll.setBackground(Color.WHITE);

    return scroll;
  }
}
