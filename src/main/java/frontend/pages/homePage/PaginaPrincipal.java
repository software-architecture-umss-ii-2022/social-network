package frontend.pages.homePage;

import backend.serviciousuarios.ServicioUsuarios;
import frontend.components.organisms.ListaPublicacionCard;
import frontend.global.Accion;
import frontend.global.SesionActual;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.util.Optional;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PaginaPrincipal extends JPanel {

  private int usuario;
  private final Color backgroundColor;
  private JScrollPane scrollPane;
  private ListaPublicacionCard listaPublicacionCard;
  private JPanel main;
  private JTextArea post;
  private JPanel userInformation;
  private JPanel mainPanel;

  public PaginaPrincipal(int usuario) {
    this.usuario = usuario;
    this.backgroundColor = new Color(234, 213, 220);
    configurePanel();
  }

  private void configurePanel() {
    this.mainPanel = createMainPanel();
    add(mainPanel);
  }

  private JPanel createMainPanel() {

    JPanel panel = new JPanel(new BorderLayout());
    this.userInformation = createUserInformation();
    panel.add(userInformation, BorderLayout.NORTH);
    this.main = createPublications();
    panel.add(main, BorderLayout.CENTER);
    return panel;
  }

  private JPanel createPublications() {
    this.listaPublicacionCard = new ListaPublicacionCard(usuario);
    this.scrollPane = new JScrollPane(listaPublicacionCard);

    JPanel panel = new JPanel(new BorderLayout());
    panel.add(createPublicationButton(), BorderLayout.NORTH);
    panel.add(scrollPane, BorderLayout.CENTER);
    Rectangle rectangle = new Rectangle(0, 80, 10, 10);
    listaPublicacionCard.scrollRectToVisible(rectangle);
    panel.setBackground(backgroundColor);
    return panel;
  }

  private JButton createPublicationButton() {
    JButton button = new JButton("Create Publication");

    ActionListener listener = e -> {
      JOptionPane.showMessageDialog(
          this,
          publicationForm(),
          "Create Publication",
          JOptionPane.PLAIN_MESSAGE
      );
      String content = post.getText();
      Accion.getInstance().getServicioPublicaciones().agregarPublicacion(usuario, content);

      mainPanel.remove(main);
      this.main = createPublications();
      mainPanel.add(main, BorderLayout.CENTER);
      mainPanel.repaint();
      mainPanel.revalidate();
    };
    button.addActionListener(listener);
    return button;
  }

  private JPanel publicationForm() {
    JPanel panel = new JPanel(new GridLayout(0, 1));
    post = new JTextArea("I'm thinking...", 2, 1);
    JScrollPane jScrollPane = new JScrollPane(post);
    panel.add(new JLabel("Publication:"));
    panel.add(jScrollPane);

    return panel;
  }

  private JPanel createUserInformation() {
    JPanel panel = new JPanel();
    String name = Accion.getInstance().getServicioUsuarios().buscarUsuario(usuario).getNombre();

    panel.setLayout(new BorderLayout());
    panel.add(new JLabel(name), BorderLayout.WEST);
    panel.add(changeUserButton(), BorderLayout.EAST);
    return panel;
  }

  private JButton changeUserButton() {
    JButton button = new JButton("cambiar usuario");

    ActionListener listener = e -> {
      String nombre = JOptionPane.showInputDialog(this, "Usuario");
      ServicioUsuarios servicioUsuarios = Accion.getInstance().getServicioUsuarios();
      Optional<Integer> idUsuario = servicioUsuarios.listarUsuarios().stream()
          .filter(currentId -> servicioUsuarios.buscarUsuario(currentId).getNombre().equals(nombre))
          .findFirst();
      if (idUsuario.isEmpty()) {
        JOptionPane.showMessageDialog(this,
            "Usuario no existe",
            "Error",
            JOptionPane.ERROR_MESSAGE);
      } else {
        remove(mainPanel);
        this.usuario = idUsuario.get();
        SesionActual.getInstance().setCurrentUser(usuario);
        this.mainPanel = createMainPanel();
        add(mainPanel);
        repaint();
        revalidate();
      }
    };
    button.addActionListener(listener);
    return button;
  }
}
