package frontend.pages.authenticationPage;

import backend.serviciousuarios.ServicioUsuarios;
import frontend.global.Accion;
import frontend.global.SesionActual;
import frontend.pages.homePage.PaginaPrincipal;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PaginaAutentificacion extends JPanel {
  private final int width;
  private final int height;

  private JLabel label;
  private JTextField field;

  public PaginaAutentificacion() {
    this.width = 300;
    this.height = 300;
    configureFrame();
  }

  public void configureFrame() {

    label = new JLabel("User");
    label.setBounds(10, 20, 80,25);
    add(label);

    field = new JTextField(20);
    field.setBounds(100, 20, 165, 25);
    add(field);

    add(searchButton());
    //setLocationRelativeTo(null);
    //setResizable(false);
    //setVisible(true);
  }

  private JButton searchButton() {
    JButton button = new JButton("Register");
    button.setBounds(10, 80, 80, 25);

    ActionListener listener = e -> {
      String username = field.getText();
      ServicioUsuarios servicioUsuarios = Accion.getInstance().getServicioUsuarios();
      int idUsuario = servicioUsuarios.agregarUsuario(username);
      SesionActual.getInstance().setCurrentUser(idUsuario);
      removeAll();
      add(new PaginaPrincipal(idUsuario));
      revalidate();
      repaint();
    };
    button.addActionListener(listener);
    return button;
  }
}
