package backend.infrastructure;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

import backend.serviciopublicaciones.infrastructure.PublicacionRepositorio;
import backend.serviciopublicaciones.Publicacion;
import backend.serviciousuarios.Usuario;
import backend.serviciopublicaciones.infrastructure.PublicacionRepositorioImpl;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;

//@ExtendWith(MockitoExtension.class)
class PublicacionRepositoryImplTest {

  private static final SimpleDateFormat SIMPLE_DATE_FORMAT =
      new SimpleDateFormat("dd/MM/yyyy");

  private static final UUID USER_UUID1 = UUID.fromString("d1b4121a-c90b-43b6-aa82-3988051a0ec9");
  private static final String USER_NAME1 = "Test Name User 1";
  private static final String USER_LASTNAME1 = "Test Lastname User 1";
  private static final String USER_USERNAME1 = "Test username User 1";
  private static final String USER_BIRTHDAY_STRING1 = "28/07/1997";
  private Date userBirthday1;
  private Usuario usuario1;

  private static final UUID PUBLICATION_UUID1 =
      UUID.fromString("42c709e9-9175-43b3-b66e-f5da2f61dd26");
  private static final String PUBLICATION_CONTENT1 = "Testing Content 1";
  private static final String DATE_STRING1 = "25/12/2022";
  private Date publicationDate1;
  private Publicacion publicacion1;

  private static final UUID USER_UUID2 = UUID.fromString("c7f03f67-5588-49a5-8f27-5fc7403e2b8b");
  private static final String USER_NAME2 = "Test Name User 2";
  private static final String USER_LASTNAME2 = "Test Lastname User 2";
  private static final String USER_USERNAME2 = "Test username User 2";
  private static final String USER_BIRTHDAY_STRING2 = "03/03/1975";
  private Date userBirthday2;
  private Usuario usuario2;

  private static final UUID PUBLICATION_UUID2 =
      UUID.fromString("4f5b8b39-86b6-43fa-b9e7-ccd9c0791e0d");
  private static final String PUBLICATION_CONTENT2 = "Testing Content 2";
  private static final String DATE_STRING2 = "01/10/2008";
  private Date publicationDate2;
  private Publicacion publicacion2;

  private static final String LINE1 = PUBLICATION_UUID1 + ","
      + USER_UUID1 + ","
      + PUBLICATION_CONTENT1 + ","
      + DATE_STRING1;

  @Mock
  private File file;
  @Mock
  private Scanner input;
  @Mock
  private FileWriter output;

  @InjectMocks
  private PublicacionRepositorio publicacionRepositorio;

  /*
  @BeforeEach
  void setUp() {
    try {
      publicationDate1 = SIMPLE_DATE_FORMAT.parse(DATE_STRING1);
      publicationDate2 = SIMPLE_DATE_FORMAT.parse(DATE_STRING2);
      userBirthday1 = SIMPLE_DATE_FORMAT.parse(USER_BIRTHDAY_STRING1);
      userBirthday2 = SIMPLE_DATE_FORMAT.parse(USER_BIRTHDAY_STRING2);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
    usuario1 = new Usuario(USER_UUID1, USER_NAME1, USER_LASTNAME1, USER_USERNAME1, userBirthday1);
    publicacion1 = new Publicacion(PUBLICATION_UUID1, usuario1, PUBLICATION_CONTENT1, publicationDate1);
    usuario2 = new Usuario(USER_UUID2, USER_NAME2, USER_LASTNAME2, USER_USERNAME2, userBirthday2);
    publicacion2 = new Publicacion(PUBLICATION_UUID2, usuario2, PUBLICATION_CONTENT2, publicationDate2);
  }
  */

  /*
  @Test
  void save() {
    Mockito.when(input.hasNextLine()).thenReturn(true);
    Mockito.when(input.nextLine()).thenReturn(LINE1);
    Mockito.when(publicationRepository.save(any(Publication.class)))
        .thenReturn(publication1);
    Publication result = publicationRepository.save(publication1);
    assertEquals(PUBLICATION_UUID1, result.getId());
    assertEquals(PUBLICATION_CONTENT1, result.getContent());
    assertEquals(publicationDate1, result.getPublicationDate());
    assertEquals(LINE1, publication1.toCSV());
  }
  */

  /*
  @Test
  void save() throws IOException {
    //Mockito.doNothing().when(output).write(any(String.class));
    Mockito.doThrow(new RuntimeException()).when(output).write(any(String.class));
    Publicacion result = publicacionRepositorio.save(publicacion1);
    assertEquals(PUBLICATION_UUID1, result.getId());
    assertEquals(PUBLICATION_CONTENT1, result.getContenido());
    assertEquals(publicationDate1, result.getPublicationDate());
    assertEquals(LINE1, publicacion1.toCSV());
  }
  */

  /*
  @Test
  void saveInFile() throws IOException {
    PublicacionRepositorio repository = new PublicacionRepositorioImpl();
    Publicacion result = repository.save(publicacion1);
    assertEquals(PUBLICATION_UUID1, result.getId());
    assertEquals(PUBLICATION_CONTENT1, result.getContenido());
    assertEquals(publicationDate1, result.getPublicationDate());
    assertEquals(LINE1, publicacion1.toCSV());
  }
  */

  /*
  @Test
  void getById() {
    PublicacionRepositorio repository = new PublicacionRepositorioImpl();
    repository.save(publicacion1);
    repository.save(publicacion2);

    Publicacion result1 = repository.findById(PUBLICATION_UUID1)
        .orElseThrow(() -> new RuntimeException("Publication1 Not Found"));
    Publicacion result2 = repository.findById(PUBLICATION_UUID2)
        .orElseThrow(() -> new RuntimeException("Publication2 Not Found"));
    assertEquals(PUBLICATION_UUID1, result1.getId());
    assertEquals(PUBLICATION_UUID2, result2.getId());
    assertEquals(PUBLICATION_CONTENT1, result1.getContenido());
    assertEquals(PUBLICATION_CONTENT2, result2.getContenido());
    assertEquals(publicationDate1, result1.getPublicationDate());
    assertEquals(publicationDate2, result2.getPublicationDate());
    assertEquals(LINE1, publicacion1.toCSV());
  }
  */

  /*
  @Test
  void getAll() {
    PublicacionRepositorio repository = new PublicacionRepositorioImpl();
    repository.save(publicacion1);
    repository.save(publicacion2);

    List<Publicacion> list = repository.getAll();
    assertEquals(PUBLICATION_UUID1, list.get(0).getId());
    assertEquals(PUBLICATION_CONTENT1, list.get(0).getContenido());
    assertEquals(2, list.size());

    assertEquals(PUBLICATION_UUID1, result.getId());
    assertEquals(PUBLICATION_CONTENT1, result.getContent());
    assertEquals(publicationDate1, result.getPublicationDate());
    assertEquals(LINE1, publication1.toCSV());
  }
  */

  /*
  @Test
  void deleteById() {
    PublicationRepository repository = new PublicationRepositoryImpl();
    repository.save(publication1);
    repository.save(publication2);

    Publication result1 = repository.deleteById(PUBLICATION_UUID1)
        .orElseThrow(() -> new RuntimeException("Entity Not Found"));
    Publication result2 = repository.deleteById(PUBLICATION_UUID2)
        .orElseThrow(() -> new RuntimeException("Entity Not Found"));
    List<Publication> list = repository.getAll();
    assertEquals(0, list.size());
  }
  */
}