package backend.infrastructure;

import backend.serviciousuarios.Usuario;
import backend.serviciousuarios.infrastructure.UsuarioRepositorioImpl;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UsuarioRepositoryImplTest {

  private static final SimpleDateFormat SIMPLE_DATE_FORMAT =
      new SimpleDateFormat("dd/MM/yyyy");

  private static final UUID USER_UUID1 = UUID.fromString("0bd8d412-94a8-4dbc-a70d-304ad2993007");
  private static final String USER_NAME1 = "Maria";
  private static final String USER_LASTNAME1 = "Jimenez";
  private static final String USER_USERNAME1 = "maruja";
  private static final String USER_BIRTHDAY_STRING1 = "26/07/1970";
  private Date userBirthday1;
  private Usuario usuario1;

  private static final UUID USER_UUID2 = UUID.fromString("07e9ccc8-9ec9-4f3c-8463-1c4a58f828a2");
  private static final String USER_NAME2 = "Jose Enrique";
  private static final String USER_LASTNAME2 = "Camacho Silvestre";
  private static final String USER_USERNAME2 = "enrique";
  private static final String USER_BIRTHDAY_STRING2 = "28/07/1997";
  private Date userBirthday2;
  private Usuario usuario2;

  private static final String LINE1 = USER_UUID1 + ","
      + USER_NAME1 + ","
      + USER_LASTNAME1 + ","
      + USER_USERNAME1 + ","
      + USER_BIRTHDAY_STRING1;

  /*
  @BeforeEach
  void setUp() {
    try {
      userBirthday1 = SIMPLE_DATE_FORMAT.parse(USER_BIRTHDAY_STRING1);
      userBirthday2 = SIMPLE_DATE_FORMAT.parse(USER_BIRTHDAY_STRING2);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
    usuario1 = new Usuario(USER_UUID1, USER_NAME1, USER_LASTNAME1, USER_USERNAME1, userBirthday1);
    usuario2 = new Usuario(USER_UUID2, USER_NAME2, USER_LASTNAME2, USER_USERNAME2, userBirthday2);
  }
  */

  /*
  @Test
  void save() throws IOException {
    UsuarioRepositorioImpl userRepository = new UsuarioRepositorioImpl();
    Usuario result = userRepository.save(usuario1);
    assertEquals(USER_UUID1, result.getId());
    assertEquals(USER_NAME1, result.getName());
    assertEquals(USER_USERNAME1, result.getUsername());
    assertEquals(LINE1, usuario1.toCSV());
  }
  */

  @Test
  void findById() {
  }

  @Test
  void deleteById() {
  }

  /*
  @Test
  void findByUsername() throws IOException {
    UsuarioRepositorioImpl userRepository = new UsuarioRepositorioImpl();
    userRepository.save(usuario1);
    userRepository.save(usuario2);
    Usuario result = userRepository.findByUsername(USER_USERNAME1).orElseThrow();
    assertEquals(USER_UUID1, result.getId());
    assertEquals(USER_NAME1, result.getName());
    assertEquals(USER_USERNAME1, result.getUsername());
    assertEquals(LINE1, usuario1.toCSV());
  }
  */

  @Test
  void getByName() {
  }

  @Test
  void getAll() {
  }
}